# Import the libraries
import numpy as np
import matplotlib.pyplot as plt  # for 畫圖用
import pandas as pd
# Feature Scaling
from sklearn.preprocessing import MinMaxScaler
# Import the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import GRU
from keras.layers import CuDNNLSTM
from keras.layers import Dropout
from keras.models import load_model
from keras import optimizers
# Obtain datetime from python
from datetime import datetime

import os.path

def trim_dataset(mat, batch_size):
    """
    trims dataset to a size that's divisible by BATCH_SIZE
    """
    no_of_rows_drop = mat.shape[0]%batch_size
    if(no_of_rows_drop > 0):
        return mat[no_of_rows_drop:]
    else:
        return mat

def cal_change(series):
    change = []
    change.append(0)
    for i in range(1, series.shape[0]):
        change.append((series[i] - series[i-1])/series[i])
    change = np.array(change)
    change = np.reshape(change, (change.shape[0], 1))
    return change

def cal_highlow(high, low):
    highLow = []
    for i in range(0, high.shape[0]):
        highLow.append(high[i] - low[i])
    highLow = np.array(highLow)
    highLow = np.reshape(highLow, (highLow.shape[0], 1))
    return  highLow

def cal_backdateHigh(series, backdate):
    high = []
    for i in range(0, series.shape[0]):
        if(backdate > i):
            high.append(np.amax(series[0:i+1]))
        else:
            high.append(np.amax(series[i - backdate:i]))
    high = np.array(high)
    high = np.reshape(high, (high.shape[0], 1))
    return high

def cal_backdateLow(series, backdate):
    low = []
    for i in range(0, series.shape[0]):
        if(backdate > i):
            low.append(np.amin(series[0:i+1]))
        else:
            low.append(np.amin(series[i - backdate:i]))
    low = np.array(low)
    low = np.reshape(low, (low.shape[0], 1))
    return low

def cal_futureHigh(series, futuredate):
    high = []
    for i in range(0, series.shape[0]):
        if(i+futuredate < series.shape[0]):
            high.append(np.amax(series[i:i+futuredate]))
        else:
            high.append(np.amax(series[i:series.shape[0]]))
    high = np.array(high)
    high = np.reshape(high, (high.shape[0], 1))
    return high

def cal_futureLow(series, futuredate):
    low = []
    for i in range(0, series.shape[0]):
        if(i+futuredate < series.shape[0]):
            low.append(np.amin(series[i:i+futuredate]))
        else:
            low.append(np.amin(series[i:series.shape[0]]))
    low = np.array(low)
    low = np.reshape(low, (low.shape[0], 1))
    return low

def gen_trainingSet(trainingSet, trainingBackdate, trainingFuture):
    change = cal_change(trainingSet[:, 0])
    volChange = cal_change(trainingSet[:, 4])
    highLow = cal_highlow(trainingSet[:, 2], trainingSet[:, 3])
    backdate_high = cal_backdateHigh(trainingSet[:, 2], trainingBackdate)
    backdate_low = cal_backdateLow(trainingSet[:, 3], trainingBackdate)
    future_high = cal_futureHigh(trainingSet[:, 2], trainingFuture)
    future_low = cal_futureLow(trainingSet[:, 3], trainingFuture)
    trainingSet = np.append(trainingSet, change, axis=1)
    trainingSet = np.append(trainingSet, volChange, axis=1)
    trainingSet = np.append(trainingSet, highLow, axis=1)
    trainingSet = np.append(trainingSet, backdate_high, axis=1)
    trainingSet = np.append(trainingSet, backdate_low, axis=1)
    trainingSet = np.append(trainingSet, future_high, axis=1)
    trainingSet = np.append(trainingSet, future_low, axis=1)
    for i in range(0, trainingSet.shape[0]):
        trainingSet[i].astype('float32')
    return trainingSet

def gen_XtrainYtrain(trainingSet):
    Xscaler = {}
    Yscaler = MinMaxScaler(feature_range=(0, 1))

    Xtrain = []
    Ytrain = []

    for i in range(loopback, trainingSet.shape[0]):
        Xtrain.append(trainingSet[i - loopback: i - 1, 0:10])
        Ytrain.append(trainingSet[i, 10:12])
    Xtrain, Ytrain = np.array(Xtrain), np.array(Ytrain)
    for i in range(0, Xtrain.shape[2]):
        Xscaler[i] = MinMaxScaler(feature_range=(0, 1))
        Xtrain[:, :, i] = Xscaler[i].fit_transform(Xtrain[:, :, i])

    Ytrain = Yscaler.fit_transform(Ytrain)

    Xtrain = np.reshape(Xtrain, (Xtrain.shape[0], Xtrain.shape[1], Xtrain.shape[2]))
    Xtrain, Ytrain = trim_dataset(Xtrain, batchSize), trim_dataset(Ytrain, batchSize)
    return Xtrain, Ytrain, Xscaler, Yscaler

def gen_lstmModel(Xtrain, Ytrain):
    # Initialising the RNN
    regressor = Sequential()

    # Adding the first LSTM layer and some Dropout regularisation
    regressor.add(GRU(units=32,
                       return_sequences=True,
                       batch_input_shape=(batchSize, loopback - 1, Xtrain.shape[2]),
                       dropout=0.0,
                       recurrent_dropout=0.0,
                       stateful=True, kernel_initializer='random_uniform'))
    regressor.add(Dropout(0.2))
    regressor.add(GRU(units=32, return_sequences=True, stateful=True))
    regressor.add(Dropout(0.2))
    regressor.add(GRU(units=32, return_sequences=True, stateful=True))
    regressor.add(Dropout(0.2))
    regressor.add(GRU(units=32, stateful=True))
    regressor.add(Dropout(0.2))
    regressor.add(Dense(20, activation='relu'))
    regressor.add(Dense(2, activation='sigmoid'))

    # Compiling
    regressor.compile(optimizer='adam', loss='mean_squared_error')

    # 進行訓練
    regressor.fit(Xtrain, Ytrain, epochs=epochs, batch_size=batchSize)
    return regressor

#def gen_prediction()

dataSet = pd.read_csv("C:\\Projects\\StockPrediction_2\\data\\YM_20190728165435.csv",
                      header=0, infer_datetime_format=True, parse_dates=['Date'], index_col='Date')
batchSize = 32 # Size of each test data, e.g. 32 = test data length is 32, test data will be [x1,x2,x3,x4,x5,x6,x7,x8,...,x32]
loopback = 4 * 8 # Y = loopback date + 1, e.g. loopback=24, Y = prediction of 1st - 23th dataset
epochs = 1000 # number of loop to produce the regression model
genNewModel = False # flag to gen new model, if false, turns to prediction mode
modelFileName = 'YM_5min_20190729003547_loopback=32.h5' # saved model name, not useful when genNewModel = True
trainingBackdate = 4 * 8 # parameter of X train data
trainingFuture = 4 * 8 # parameter of Y training data
buffer = 0 # buffer for back testing parameter

trainingSet = dataSet.values
if(genNewModel != True):
    trainingSet = trainingSet[-30*8:]
trainingSet = gen_trainingSet(trainingSet, trainingBackdate, trainingFuture)

Xtrain, Ytrain, Xscaler, Yscaler = gen_XtrainYtrain(trainingSet)


if (os.path.isfile(modelFileName) and genNewModel == False):
    regressor = load_model(modelFileName)
else:
    regressor = gen_lstmModel(Xtrain, Ytrain)
    currentTime = datetime.now().strftime('%Y%m%d%H%M%S')
    regressor.save("test_"+currentTime+".h5")

predictedResult = regressor.predict(Xtrain)
predictedResult = Yscaler.inverse_transform(predictedResult)
trainingSet = trainingSet[loopback:]

high = predictedResult[:, 0]
low = predictedResult[:, 1]
hsi = trainingSet[:, 0]

if(genNewModel != True):
    predictionArr = []
    testdata = []
    for i in range(0, Xtrain.shape[0] - batchSize):
        data = Xtrain[0: batchSize+i]
        data = trim_dataset(data, batchSize)
        testdata.append(data)
        prediction = regressor.predict(data)
        prediction = prediction[prediction.shape[0]-1]
        predictionArr.append(prediction)
    predictionArr = Yscaler.inverse_transform(predictionArr)

    high = predictionArr[:, 0]
    low = predictionArr[:, 1]

status = "none"
earning = 0
earningAccu = []
opening = 0
closing = 0


# hsi = trim_dataset(hsi, batchSize)
hsi = hsi[-high.shape[0]:]

for i in range(0, hsi.shape[0]):
    if (status == "short"):
        earning = earning - (hsi[i] - closing)
    if (status == "long"):
        earning = earning + (hsi[i] - closing)
    if (hsi[i] - buffer >= high[i] and status != "short"):
        status = "short"
        opening = hsi[i]
        # earning = earning - 2
    if (hsi[i] + buffer <= low[i] and status != "long"):
        status = "long"
        opening = hsi[i]
        # earning = earning - 2
    earningAccu.append(earning)
    closing = hsi[i]


fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

# Visualising the results
ax1.plot(hsi, color = 'red', label = 'Real Index')
ax1.plot(high, color = 'blue', label = 'Predicted high')
ax1.plot(low, color = 'blue', label = 'Predicted low')
#plt.plot(highActual, color = 'yellow', label = 'Actual high', linestyle='dashed')
#plt.plot(lowActual, color = 'yellow', label = 'Actual low', linestyle='dashed')
ax2.plot(earningAccu, color = 'green', label = 'Earning')
plt.title('Index Prediction')
plt.xlabel('Time')
plt.ylabel('Index')
plt.legend()
plt.show()
# print(earningAccu)



