# Import the libraries
import numpy as np
import matplotlib.pyplot as plt  # for 畫圖用
import pandas as pd
# Feature Scaling
from sklearn.preprocessing import MinMaxScaler
# Import the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import GRU
from keras.layers import CuDNNLSTM
from keras.layers import Dropout
from keras.models import load_model
from keras import optimizers
# Obtain datetime from python
from datetime import datetime
from argparse import ArgumentParser
import requests
import mysql.connector

import os.path

def trim_dataset(mat, batch_size):
    """
    trims dataset to a size that's divisible by BATCH_SIZE
    """
    no_of_rows_drop = mat.shape[0]%batch_size
    if(no_of_rows_drop > 0):
        return mat[no_of_rows_drop:]
    else:
        return mat

def cal_change(series):
    change = []
    change.append(0)
    for i in range(1, series.shape[0]):
        change.append((series[i] - series[i-1])/series[i])
    change = np.array(change)
    change = np.reshape(change, (change.shape[0], 1))
    return change

def cal_highlow(high, low):
    highLow = []
    for i in range(0, high.shape[0]):
        highLow.append(high[i] - low[i])
    highLow = np.array(highLow)
    highLow = np.reshape(highLow, (highLow.shape[0], 1))
    return  highLow

def cal_backdateHigh(series, backdate):
    high = []
    for i in range(0, series.shape[0]):
        if(backdate > i):
            high.append(np.amax(series[0:i+1]))
        else:
            high.append(np.amax(series[i - backdate:i]))
    high = np.array(high)
    high = np.reshape(high, (high.shape[0], 1))
    return high

def cal_backdateLow(series, backdate):
    low = []
    for i in range(0, series.shape[0]):
        if(backdate > i):
            low.append(np.amin(series[0:i+1]))
        else:
            low.append(np.amin(series[i - backdate:i]))
    low = np.array(low)
    low = np.reshape(low, (low.shape[0], 1))
    return low

def cal_futureHigh(series, futuredate):
    high = []
    for i in range(0, series.shape[0]):
        if(i+futuredate < series.shape[0]):
            high.append(np.amax(series[i:i+futuredate]))
        else:
            high.append(np.amax(series[i:series.shape[0]]))
    high = np.array(high)
    high = np.reshape(high, (high.shape[0], 1))
    return high

def cal_futureLow(series, futuredate):
    low = []
    for i in range(0, series.shape[0]):
        if(i+futuredate < series.shape[0]):
            low.append(np.amin(series[i:i+futuredate]))
        else:
            low.append(np.amin(series[i:series.shape[0]]))
    low = np.array(low)
    low = np.reshape(low, (low.shape[0], 1))
    return low

def gen_trainingSet(trainingSet, trainingBackdate, trainingFuture):
    change = cal_change(trainingSet[:, 0])
    volChange = cal_change(trainingSet[:, 4])
    highLow = cal_highlow(trainingSet[:, 2], trainingSet[:, 3])
    backdate_high = cal_backdateHigh(trainingSet[:, 2], trainingBackdate)
    backdate_low = cal_backdateLow(trainingSet[:, 3], trainingBackdate)
    future_high = cal_futureHigh(trainingSet[:, 2], trainingFuture)
    future_low = cal_futureLow(trainingSet[:, 3], trainingFuture)
    trainingSet = np.append(trainingSet, change, axis=1)
    trainingSet = np.append(trainingSet, volChange, axis=1)
    trainingSet = np.append(trainingSet, highLow, axis=1)
    trainingSet = np.append(trainingSet, backdate_high, axis=1)
    trainingSet = np.append(trainingSet, backdate_low, axis=1)
    trainingSet = np.append(trainingSet, future_high, axis=1)
    trainingSet = np.append(trainingSet, future_low, axis=1)
    for i in range(0, trainingSet.shape[0]):
        trainingSet[i].astype('float32')
    return trainingSet

def gen_XtrainYtrain(trainingSet):
    Xscaler = {}
    Yscaler = MinMaxScaler(feature_range=(0, 1))

    Xtrain = []
    Ytrain = []

    for i in range(loopback, trainingSet.shape[0]):
        Xtrain.append(trainingSet[i - loopback: i - 1, 0:10])
        Ytrain.append(trainingSet[i, 10:12])
    Xtrain, Ytrain = np.array(Xtrain), np.array(Ytrain)
    for i in range(0, Xtrain.shape[2]):
        Xscaler[i] = MinMaxScaler(feature_range=(0, 1))
        Xtrain[:, :, i] = Xscaler[i].fit_transform(Xtrain[:, :, i])

    Ytrain = Yscaler.fit_transform(Ytrain)

    Xtrain = np.reshape(Xtrain, (Xtrain.shape[0], Xtrain.shape[1], Xtrain.shape[2]))
    Xtrain, Ytrain = trim_dataset(Xtrain, batchSize), trim_dataset(Ytrain, batchSize)
    return Xtrain, Ytrain, Xscaler, Yscaler

def runQuery (db, sql):
    cursor = db.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    return result

def runNonQuery (db, sql, val=""):
    cursor = db.cursor()
    cursor.execute(sql, val)
    db.commit()
    print(cursor.rowcount, "record inserted.")

def trade(db, assetName, high, low, close):
    quantity = 0
    newDecision = ""
    currentDecision = ""
    if(close >= high):
        newDecision = "sell"
        quantity = -1
    if(close <= low):
        newDecision = "buy"
        quantity = 1
    sql = "select count(*) from transaction where asset = '"+assetName+"' and close is null"
    cnt = runQuery(db, sql)[0][0]
    if(cnt == 0): # No holding position
        sql = "INSERT INTO transaction (txdate, asset, quantity, open) VALUES (%s, %s, %s, %s)"
        val = (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), assetName, quantity, close)
        runNonQuery(db, sql, val)
    if(cnt != 0):
        if(newDecision == "sell"):
            sql = "select count(*) from transaction where asset = '" + assetName + "' and quantity = 1 and close is null"
            if(runQuery(db, sql)[0][0] != 0):
                currentDecision = "buy"
            if(newDecision != currentDecision):
                sql = "update transaction set close = "+close+", earning = " + close + "-open where asset = '" + assetName + "' and close is null"
                runNonQuery(db, sql)
                sql = "INSERT INTO transaction (txdate, asset, quantity, open) VALUES (%s, %s, %s, %s)"
                val = (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), assetName, quantity, close)
                runNonQuery(db, sql, val)
        if (newDecision == "buy"):
            sql = "select count(*) from transaction where asset = '" + assetName + "' and quantity = -1 and close is null"
            if (runQuery(db, sql)[0][0] != 0):
                currentDecision = "sell"
            if (newDecision != currentDecision):
                sql = "update transaction set close = " + close + ", earning = open-" + close + "  where asset = '" + assetName + "' and close is null"
                runNonQuery(db, sql)
                sql = "INSERT INTO transaction (txdate, asset, quantity, open) VALUES (%s, %s, %s, %s)"
                val = (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), assetName, quantity, close)
                runNonQuery(db, sql, val)

parser = ArgumentParser()
parser.add_argument("-m", "--model", help="Regression model filename")
parser.add_argument("-f", "--file", help="Test filename, rownum should larger than 32")
parser.add_argument("-l", "--loopback", default=32, help="Loopback date for each Y test data", type=int)
parser.add_argument("-n", "--name", help="Name of Asset")
args = parser.parse_args()


# print(args.loopback)
dataSet = pd.read_csv("C:\\Projects\\StockPrediction_2\\data\\"+args.file,
                      header=0, infer_datetime_format=True, parse_dates=['Date'], index_col='Date')
batchSize = 32
loopback = args.loopback
modelFileName = 'C:\\Projects\\StockPrediction_2\\model\\LSTM\\'+args.model
trainingBackdate = loopback
trainingFuture = loopback
assetName = args.name
buffer = 0

trainingSet = dataSet.values
# trainingSet = trainingSet[-loopback:]
trainingSet = gen_trainingSet(trainingSet, trainingBackdate, trainingFuture)

Xtrain, Ytrain, Xscaler, Yscaler = gen_XtrainYtrain(trainingSet)

regressor = load_model(modelFileName)
predictedResult = regressor.predict(Xtrain)
predictedResult = Yscaler.inverse_transform(predictedResult)


predictedResult = predictedResult[-1]
currentPosition = trainingSet[-1:, 0]

# print("predicted highlow: " + str(predictedResult))
# print("current position: " + str(currentPosition))


token = '702975103:AAELHmQXjPigXpf9KWWGKxuMWEXKPYYi-bs'
url = "https://api.telegram.org/bot"+token+"/sendMessage" # ?chat_id=[MY_CHANNEL_NAME]&text=[MY_MESSAGE_TEXT]
channelName = "-1001400293516"
print(url)
rowDt = dataSet._stat_axis[-1]
msg = "Datetime: " + str(rowDt) + "\r\n" +\
      "Asset: " + assetName + "\r\n" +\
      "predicted high: " + str(predictedResult[0]) + "\r\n" +\
      "current position: " + str(currentPosition[0]) + "\r\n" +\
      "predicted low: " + str(predictedResult[1])

my_params = {"chat_id": channelName, 'text': msg}
r = requests.get(url, params = my_params)
print(r.url)
print(r.status_code)

db = mysql.connector.connect(
    host="192.168.1.11",
    user="tunneltest",
    passwd="tunneltest",
    database="tunnel"
)

sql = "INSERT INTO prediction (txdate, asset, predict_high, predict_low, actual_close) VALUES (%s, %s, %s, %s, %s)"
val = (str(rowDt), assetName, str(predictedResult[0]), str(predictedResult[1]), str(currentPosition[0]))
runNonQuery(db, sql, val)

trade(db, assetName, str(predictedResult[0]), str(predictedResult[1]), str(currentPosition[0]))