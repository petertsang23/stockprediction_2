# Import the libraries
import numpy as np
import matplotlib.pyplot as plt  # for 畫圖用
import pandas as pd
# Feature Scaling
from sklearn.preprocessing import MinMaxScaler
# Import the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.models import load_model
from keras import optimizers
# Obtain datetime from python
from datetime import datetime

import os.path

def trim_dataset(mat, batch_size):
    """
    trims dataset to a size that's divisible by BATCH_SIZE
    """
    no_of_rows_drop = mat.shape[0]%batch_size
    if(no_of_rows_drop > 0):
        return mat[:-no_of_rows_drop]
    else:
        return mat

dataSet = pd.read_csv("C:\\Projects\\StockPrediction_2\\data\\HKG.IDXHKD_Candlestick_1_Hour_BID_14.06.2017-12.07.2019.csv",
                      header=0, infer_datetime_format=True, parse_dates=['Date'], index_col='Date')
batchSize = 32
loopback = 7*8
epochs = 500

trainingSet = dataSet.values

for i in range(0, trainingSet.shape[0]):
    trainingSet[i].astype('float32')

Xscaler = {}
Yscaler = MinMaxScaler(feature_range= (0, 1))

Xtrain = []
Ytrain = []

for i in range(loopback, trainingSet.shape[0]):
    Xtrain.append(trainingSet[i - loopback : i - 1, 0:10])
    Ytrain.append(trainingSet[i, 10:12])
Xtrain, Ytrain = np.array(Xtrain), np.array(Ytrain)
for i in range(0, Xtrain.shape[2]):
    Xscaler[i] = MinMaxScaler(feature_range=(0, 1))
    Xtrain[:, :, i] = Xscaler[i].fit_transform(Xtrain[:, :, i])

Ytrain = Yscaler.fit_transform(Ytrain)

Xtrain = np.reshape(Xtrain, (Xtrain.shape[0], Xtrain.shape[1], Xtrain.shape[2]))
Xtrain, Ytrain = trim_dataset(Xtrain, batchSize), trim_dataset(Ytrain, batchSize)

modelFileName = 'channel_prediction_HSI_hourly_20190713021835.h5'
genNewModel = False

if (os.path.isfile(modelFileName) and genNewModel == False):
    regressor = load_model(modelFileName)
else:
    # Initialising the RNN
    regressor = Sequential()

    # Adding the first LSTM layer and some Dropout regularisation
    #regressor.add(LSTM(units = 50, return_sequences = True, input_shape = (Xtrain.shape[1], 1)))
    regressor.add(LSTM(units=32,
                       return_sequences=True,
                       batch_input_shape=(batchSize, loopback - 1, Xtrain.shape[2]),
                       dropout=0.0,
                       recurrent_dropout=0.0,
                       stateful=True, kernel_initializer='random_uniform'))
    #lstm_model.add(LSTM(100, batch_input_shape=(BATCH_SIZE, TIME_STEPS, x_t.shape[2]), dropout=0.0, recurrent_dropout=0.0, stateful=True, kernel_initializer='random_uniform'))
    regressor.add(Dropout(0.2))
    regressor.add(LSTM(units=32, return_sequences=True, stateful=True))
    regressor.add(Dropout(0.2))
    regressor.add(LSTM(units=32, stateful=True))
    regressor.add(Dropout(0.2))
    # Adding a second LSTM layer and some Dropout regularisation
    #regressor.add(LSTM(units = 50, return_sequences = True))
    #regressor.add(Dropout(0.2))

    # Adding a third LSTM layer and some Dropout regularisation
    #regressor.add(LSTM(units = 50, return_sequences = True))
    #regressor.add(Dropout(0.2))

    # Adding a fourth LSTM layer and some Dropout regularisation
    #regressor.add(LSTM(units = 50))
    #regressor.add(Dropout(0.2))

    # Adding the output layer
    #regressor.add(Dense(units = 2))

    regressor.add(Dense(20, activation='relu'))
    regressor.add(Dense(2, activation='sigmoid'))

    # Compiling
    regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

    # 進行訓練
    regressor.fit(Xtrain, Ytrain, epochs = epochs, batch_size = batchSize)
    currentTime = datetime.now().strftime('%Y%m%d%H%M%S')
    regressor.save("test_"+currentTime+".h5")

predictedResult = regressor.predict(Xtrain)
predictedResult = Yscaler.inverse_transform(predictedResult)
trainingSet = trainingSet[loopback:]
#high = trainingSet[:, 10]
#low = trainingSet[:, 11]
high = predictedResult[:, 0]
low = predictedResult[:, 1]
hsi = trainingSet[:, 0]
#trainingSet = trainingSet[loopback:]

#for i in range(0, high.shape[0]):
#    high[i] = high[i] + predictedResult[i, 0]
#for i in range(0, low.shape[0]):
#    low[i] = low[i] + predictedResult[i, 1]

#dx = 1
#dpredictedResult = np.diff(predictedResult[:, 0])
#for i in range(0, dpredictedResult.shape[0]):
#    if(dpredictedResult[i] < 2 and dpredictedResult[i] > -2):
#        plt.annotate(dpredictedResult[i], xy=(i, predictedResult[i]), xytext=(i+2, predictedResult[i]),
#                     arrowprops=dict(facecolor='black', shrink=0.05),
#                     )

status = "none"
earning = 0
earningAccu = []
opening = 0
closing = 0
buffer = 0

hsi = trim_dataset(hsi, batchSize)

for i in range(0, hsi.shape[0]):
    if (status == "short"):
        earning = earning - (hsi[i] - closing)
    if (status == "long"):
        earning = earning + (hsi[i] - closing)
    if (hsi[i] >= high[i] and status != "short"):
        status = "short"
        opening = hsi[i]
        earning = earning - 2
    if (hsi[i] <= low[i] and status != "long"):
        status = "long"
        opening = hsi[i]
        earning = earning - 2
    earningAccu.append(earning)
    closing = hsi[i]

# Visualising the results
plt.plot(hsi, color = 'red', label = 'Real HSI')  # 紅線表示真實股價
#plt.plot(predictedResult, color = 'blue', label = 'Predicted HSI')  # 藍線表示預測股價
#plt.plot(dpredictedResult, color = 'green', label = 'DY(Predicted HSI)/DX')  # 藍線表示預測股價
plt.plot(high, color = 'blue', label = 'Predicted high')  # 藍線表示預測股價
plt.plot(low, color = 'blue', label = 'Predicted low')  # 藍線表示預測股價
plt.plot(earningAccu, color = 'green', label = 'Earning')  # 藍線表示預測股價
plt.title('HSI Prediction')
plt.xlabel('Time')
plt.ylabel('Hang Sang Index')
plt.legend()
plt.show()



