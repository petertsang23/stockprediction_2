import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import os.path
from keras.models import load_model


def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(0, len(dataset)-look_back-1):  # from 0 to dataset length - 1
        dataX.append(dataset[i:(i+look_back), 0]) # input day - lookback to day data to x
        dataY.append(dataset[i + look_back, 0]) # input day + 1 data to y
    return np.array(dataX), np.array(dataY) # return dataset


def load_file(filename, usecols):
    dataframe = read_csv(filename, usecols=[usecols], engine='python', skipfooter=3)
    dataset = dataframe.values
    dataset = dataset.astype('float32')
    return dataset

def create_model(X_train, Y_train):
    model = Sequential()
    # Adding the first LSTM layer and some Dropout regularisation
    #model.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
    model.add(LSTM(units=50, batch_input_shape=(1, X_train.shape[1], 1)))
    #model.add(Dropout(0.2))
    # Adding a second LSTM layer and some Dropout regularisation
    #model.add(LSTM(units=50, return_sequences=True))
    #model.add(Dropout(0.2))
    # Adding a third LSTM layer and some Dropout regularisation
    #model.add(LSTM(units=50, return_sequences=True))
    #model.add(Dropout(0.2))
    # Adding a fourth LSTM layer and some Dropout regularisation
    #model.add(LSTM(units=50))
    #model.add(Dropout(0.2))
    # Adding the output layer
    model.add(Dense(units=1))
    # Compiling
    model.compile(optimizer='adam', loss='mean_squared_error')
    # Train Model
    #model.fit(X_train, Y_train, epochs=100, batch_size=32)
    for i in range(100):
        model.fit(X_train, Y_train, epochs=1, batch_size=1, shuffle=False)
        model.reset_states()
    return model

def prediction(model, X_test):
    result = model.predict(X_test)
    result = scaler.inverse_transform(result) # to get the original scale

    #test = scaler.inverse_transform(X_test)
    #plt.plot(test, color='red', label='Real Data')  # 藍線表示預測股價
    plt.plot(result, color='blue', label='Predicted')  # 藍線表示預測股價
    plt.title('Prediction')
    plt.xlabel('Time')
    plt.ylabel('Price')
    plt.legend()
    plt.show()

model_filename = 'lstm_hsi_ms_1.h5'
loop_back = 60
if os.path.isfile(model_filename):
    model = load_model(model_filename)
else:
    original_data = load_file('HSI50_his_20140420_20190420_day_thin.csv', 1)

    dataset = load_file('HSI50_his_20140420_20190420_day_thin.csv', 1)
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = scaler.fit_transform(dataset)
    X_train, Y_train = create_dataset(dataset, loop_back)
    # convert X_train into 3-dimensions, org: time,price, to-be:[prices, timesteps, indicators]
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    model = create_model(X_train, Y_train)
    model.save(model_filename)

sp500 = load_file('SP500_his_20140420_20190420_day_thin.csv', 1)
scaler = MinMaxScaler(feature_range=(0, 1))
sp500 = scaler.fit_transform(sp500)
X_test, Y_test = create_dataset(sp500, loop_back)
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
#prediction(model, X_test)

result = model.predict(X_test)
result = scaler.inverse_transform(result) # to get the original scale

sp500 = scaler.inverse_transform(sp500)

sp500 = sp500[60:len(sp500)-1]
#test = scaler.inverse_transform(X_test)
plt.plot(sp500, color='red', label='Real Data')  # 藍線表示預測股價
plt.plot(result, color='blue', label='Predicted')  # 藍線表示預測股價
plt.title('Prediction')
plt.xlabel('Time')
plt.ylabel('Price')
plt.legend()
plt.show()