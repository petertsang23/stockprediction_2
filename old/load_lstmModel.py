# LSTM for international airline passengers problem with regression framing
import numpy
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import load_model



# 產生 (X, Y) 資料集, Y 是下一期的乘客數
def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return numpy.array(dataX), numpy.array(dataY)

# 載入模型
model = load_model('lstm_hsi.h5')

dataframe = read_csv('SP500_his_20140420_20190420_day.csv', usecols=[4], engine='python', skipfooter=3)
dataset = dataframe.values
dataset = dataset.astype('float32')
# 正規化(normalize) 資料，使資料值介於[0, 1]
scaler = MinMaxScaler(feature_range=(0, 1))
dataset = scaler.fit_transform(dataset)

look_back = 1
testX, testY = create_dataset(dataset, look_back)
testX = numpy.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
testPredict = model.predict(testX)
testPredict = scaler.inverse_transform(testPredict)

# 畫測試資料趨勢圖
# shift test predictions for plotting
testPredictPlot = numpy.empty_like(dataset)
testPredictPlot[:, :] = numpy.nan
testPredictPlot[look_back:len(testPredict)+look_back, :] = testPredict

originalPlot = scaler.inverse_transform(dataset)

plt.plot(originalPlot, label='original')
plt.plot(testPredictPlot, label='predict')
plt.legend()
plt.show()